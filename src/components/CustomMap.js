import PropTypes from "prop-types";
import React, { Component } from "react";
import {
  Map,TileLayer, Marker, Popup
} from "react-leaflet";


const MyPopupMarker = (props) => (
  <Marker position={props.position}>
    <Popup>
      {Object.keys(props).map((data, idx) => {
            return <div key={`${props.key}-${idx}`}>{`${data} : ${props[data]}`}</div>
        })}
    </Popup>
  </Marker>
);

const MyMarkersList = ({ markers }) => {
  const items = markers.map(({ key, ...props }) => (
    <MyPopupMarker key={key} {...props} />
  ));
  return <div style={{ display: "none" }}>{items}</div>;
};
MyMarkersList.propTypes = {
  markers: PropTypes.array.isRequired
};

export default class CustomMap extends Component {
  

  constructor(props) {
    super(props);
    this.state = {
      lat: 13.1935950,
      lng: 77.6491150,
      zoom: 12,
      markers: []
    };
    this.fetchData = this.fetchData.bind(this);
  }

  componentDidMount() {
    console.log("mounting");
    this.fetchData();
  }

  xmlToJson(xml) {
    // Create the return object
    var obj = {};
  
    if (xml.nodeType === 1) {
      // element
      // do attributes
      if (xml.attributes.length > 0) {
        obj.position = [];
        for (var j = 0; j < xml.attributes.length; j++) {
          var attribute = xml.attributes.item(j);
          if (Number(attribute.nodeValue)) {
            obj.position.push(Number(attribute.nodeValue));
          }
          else {
            obj.position.push(attribute.nodeValue);
          }
        }
      }
    } else if (xml.nodeType === 3) {
      // text
      obj = xml.nodeValue;
    }
  
    // do children
    // If all text nodes inside, get concatenated text from them.
    var textNodes = [].slice.call(xml.childNodes).filter(function(node) {
      return node.nodeType === 3;
    });
    if (xml.hasChildNodes() && xml.childNodes.length === textNodes.length) {
      obj = [].slice.call(xml.childNodes).reduce(function(text, node) {
        return text + node.nodeValue;
      }, "");
    } else if (xml.hasChildNodes()) {
      for (var i = 0; i < xml.childNodes.length; i++) {
        var item = xml.childNodes.item(i);
        var nodeName = item.nodeName;
        if (typeof obj[nodeName] == "undefined") {
          obj[nodeName] = this.xmlToJson(item);
        } else {
          if (typeof obj[nodeName].push == "undefined") {
            var old = obj[nodeName];
            obj[nodeName] = [];
            obj[nodeName].push(old);
          }
          obj[nodeName].push(this.xmlToJson(item));
        }
      }
    }
    return obj;
  }

  getEuclideanDistance=(prev, current)=>{
    return Math.sqrt(Math.pow(Number(prev.lat)-Number(current.lat),2)
          +Math.pow(Number(prev.lon)-Number(current.lon),2));
  }

  changeInDirection(prev, current) {
    if (prev.lat !== current.lat && prev.lon !== current.lon) {
      this.directionState = "both";
      return true;
    } else if (prev.lat !== current.lat && prev.lon === current.lon) {
      if (this.directionState === "lat") return false;
      this.directionState = "lat";
      return true;
    } else if (prev.lat === current.lat && prev.lon !== current.lon) {
      if (this.directionState === "lon") return false;
      this.directionState = "lon";
      return true;
    } else return false;
  }

  getTimeElapsed(prev, current) {
    return current - prev;
  }

  fetchTrackPoints = async (jsonObj) => {
    const { trkpt } = jsonObj.gpx.trk.trkseg;
    let trackPoints = [{
      key:"marker0",
      position: [ ...trkpt[0].position],
      ele: Number(trkpt[0].ele),
      time: 0,
    }];
    let elevation = 0;
    let lastElevation = trackPoints[0].ele;
    const startTime = new Date(trkpt[0].time).getTime();
    let lastTime = new Date(trkpt[0].time).getTime();
    this.directionState = "";
    let every5dist = 0;
    let totalTime = 0;
    let movingTime =0
    let maxSpeed = 0;
    let avgSpeed = 0;
    let totalDistance = 0;
    let lastLat = trackPoints[0].position[0];
    let lastLon = trackPoints[0].position[1];
    for (let i=1; i<trkpt.length; i++){
      const key = "marker"+i;
      const time = new Date(trkpt[i].time).getTime();
      const ele = Number(trkpt[i].ele);
      const lat = trkpt[i].position[0];
      const lon = trkpt[i].position[1];

      const timeElapsed = this.getTimeElapsed(lastTime, time);
      const dist = this.getEuclideanDistance({lat:lastLat, lon:lastLon}, {lat, lon});
      every5dist += dist;
      totalDistance += dist;
      // capturing speed 
      const speed = dist/timeElapsed;
      elevation += lastElevation - ele;

      if (dist > 0) {
        avgSpeed +=speed;
        // capturing max speed
        maxSpeed = Math.max(maxSpeed, speed);
        movingTime += timeElapsed;
      }      
      lastTime = time;
      if (every5dist > 0.01 && this.changeInDirection({lat:lastLat, lon:lastLon}, {lat, lon})) {
        every5dist = 0;
        trackPoints.push({ ele, time, key, position: [ lat, lon], timeElapsed, speed, avgSpeed });
      }
      lastLat = lat;
      lastLon = lon;
      lastElevation = ele;
    }
    // capturing total time and avgspeed
    totalTime = lastTime - startTime;
    avgSpeed /= trkpt.length;
    return { elevation, totalTime, avgSpeed, maxSpeed, movingTime, totalDistance, trackPoints};
  }
  

  fetchData = async (url) => {
    const response = await fetch("https://dl.dropboxusercontent.com/s/8nvqnasci6l76nz/Problem.gpx");
    const text = await response.text();
    const xmlNode = new DOMParser().parseFromString(text, 'text/xml');
    const jsonObj = this.xmlToJson(xmlNode);
    const promiseToGetData = new Promise(async (resolve, reject)=>{
      const { elevation, totalTime, avgSpeed, maxSpeed, movingTime, trackPoints, totalDistance} = await this.fetchTrackPoints(jsonObj); 
      resolve();
      this.setState({
        lat: trackPoints[0].position[0],
        lng: trackPoints[0].position[1],
        markers: trackPoints,
        elevation,
        totalTime,
        avgSpeed,
        maxSpeed,
        movingTime,
        totalDistance,
      });
    });
  }

  render() {
    const center = [this.state.lat, this.state.lng];
    console.log(this.state.markers);
    return (
      <Map center={center} zoom={this.state.zoom}>
        <TileLayer
          attribution="Akash Roy"
          url="http://{s}.tile.osm.org/{z}/{x}/{y}.png"
        />
        <MyMarkersList markers={this.state.markers} />
      </Map>
    );
  }
}
