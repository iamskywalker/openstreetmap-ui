import React from "react";
import CustomMap from "./components/CustomMap";

export default class App extends React.Component {
  render() {
    return (
      <div>
        <CustomMap />
      </div>
    );
  }
}
